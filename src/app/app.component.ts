import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'trabajando-con-i18n';
  defaultLanguage: string = 'es';

  constructor(private traduccion: TranslateService) {
    traduccion.setDefaultLang(this.defaultLanguage);
  }
}
