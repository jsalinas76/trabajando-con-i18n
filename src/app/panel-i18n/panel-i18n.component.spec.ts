import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelI18nComponent } from './panel-i18n.component';

describe('PanelI18nComponent', () => {
  let component: PanelI18nComponent;
  let fixture: ComponentFixture<PanelI18nComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PanelI18nComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelI18nComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
